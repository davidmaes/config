<?php

    namespace davidmaes\config;

    use stdClass;
    use UnexpectedValueException;

    class Config
    {
        private static $configs = [];

        /**
         * Loads and returns a parsed JSON configuration file from the supplied path.
         *
         * @param string $path The path of the configuration file.
         *
         * @return array|stdClass The parsed config as a stdClass or array.
         * @throws UnexpectedValueException Throws exception when the configuration file can't be read.
         */
        public function get(string $path)
        {
            if (!isset(static::$configs[$path])) {
                static::$configs[$path] = $this->load($path);
            }

            return static::$configs[$path];
        }

        /**
         * @param string $path
         * @return array|stdClass
         */
        private function load(string $path)
        {
            if (!file_exists($path)) {
                throw new UnexpectedValueException('Configuration file could not be found');
            }

            return json_decode(file_get_contents($path));
        }
    }
