<?php

    use PHPUnit\Framework\TestCase;
    use davidmaes\config\Config;

    class ConfigTest extends TestCase
    {
        /**
         * Tests whether the configuration is loaded and correct.
         */
        public function test_get()
        {
            $filename = 'test.json';
            $config = new Config();
            $values = new stdClass();
            $values->random = rand(0, 1000);

            file_put_contents($filename, json_encode($values));

            $this->assertEquals($values, $config->get($filename));

            unlink($filename);
        }

        /**
         * Tests whether an exception is thrown when there is no config file at the specified path.
         */
        public function test_get_notFound()
        {
            $this->expectException(UnexpectedValueException::class);
            $config = new Config();
            $config->get('i_do_not_exist.json');
        }
    }
